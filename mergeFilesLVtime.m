%%this files has a purpose to merge the files of the track datas 
%%the datalogger writes a new file for the same datas every time it reaches
%%the size of 32MB. That means that datas from the same id in the CAN-BUS
%%are separated into different files the datalogger

%input 1:= the user must locate the folder of the datalogger's files in the
%same repository with the script

%input 2:= the user must put the start time 

%input 3:= the user must pu the end time

%output := all data in the files merged into a file from the start to the
%end time.
function all_exported = mergeFilesLVtime(name,numOfVariables)
    
    files = dir(fullfile('./',name));
    all_exported = [];
    for i = 1 : length(files)
            files(i).name;
            %!!!!!!!new editions use the function split instead of strsplit!!!!!!!
            words = strsplit(files(i).name,'.');

            %!!!!!!this ->{} also must be changed if we change the split fun!!!!!
            fulldate = strsplit(words{2},'_');

           files(i).seconds = fulldate(length(fulldate));
            files(i).minutes = fulldate(length(fulldate)-1);
            files(i).hours = fulldate(length(fulldate)-2);
            files(i).totalTime = str2double(files(i).seconds)+str2double(files(i).minutes)*60+str2double(files(i).hours)*3600;
            files(i).var = [words{1},'_',words{2}];
            files(i).t = load(files(i).name);
            files(i).tr = getfield(files(i).t,files(i).var);
    end
    for count = 1 : numOfVariables
        firsttime = 1;
        LV_time = 0;
        LV_time_length = zeros(1,length(files));

        for i=1: length(files)

            LV_time_length(i) = files(i).tr.Description.Measurement.Length;
            %last file
            if i~=1

                LV_time = LV_time + LV_time_length(i-1);
            end
            if firsttime == 1

                exported.data = [];
                exported.times = [];
                firsttime = 0;
            end
            variable = count;
            exported.name = files(i).tr.X(variable).Raster;
            for j =1 : 1
                files(i).totalTime;
                exported.data = [exported.data files(i).tr.Y(variable).Data];
                tempTimes = zeros(1,length(files(i).tr.X(variable).Data));
                tempTimes =files(i).tr.X(variable).Data;
                for c =1:length(tempTimes)

                    tempTimes(c) = tempTimes(c) + LV_time;

                end
                %tempTimes = bsxfun(@plus, tempTimes , files(i).totalTime);
                exported.times =[exported.times tempTimes];
            end

        end
        all_exported = [all_exported exported];
    end


    save('All_track_data.mat','-v7.3','all_exported');
    
 end
    



